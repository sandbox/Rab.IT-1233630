<?php
/**
 * @file version: 2.0
 *
 * Menu callbacks for module qm_mobile
 */


/**
 * Add Regular HTML to Page
 */
function qm_mobile_htmlize($title, $content){


    $menu = l("Home", "mobile/") . " | "
            . l("Free Download", 'mobile/download') . " | "
            . l("Events", 'mobile/event');


    $header = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"'
            . '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'
            . '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">'
            . '<head>'
            . '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'
            . '<title>' . $title . '</title>'
            . '<meta http-equiv="content-language" content="en" />'
            . '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'
            . '<link rel="shortcut icon" href="/sites/default/files/favicon.ico" type="image/x-icon" />'

            . '<script type="text/javascript">'
                . "var _gaq = _gaq || [];"
                . "_gaq.push(['_setAccount', 'UA-6389275-1']);"
                . "_gaq.push(['_trackPageview']);"

                . '(function() {'
                    . "var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;"
                    . "ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';"
                    . "var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);"
                . '})();'
            . '</script>';
    
    $header .= '</head>';

    $body = '<body>'
            . '<div id = "wrapper">'
            . '<img src = "/sites/all/themes/quantweb/images/backgrounds/logo.png" alt = "Quantum Method | We are with you" />'
            . '<br />'
            . $menu
            . '<br /><br />'
            . $content
            . '</div>'
            . '</body>'
            . '</html>';
    
    return $header.$body;
}


/**
 * page callback function for module path
 */
function qm_mobile_home() {

    $title = "Quantum Method The Science of Living";
	$content = "";
    $content .= "Welcome to Quantum Method Site mobile version. You can visit the original site: www.quantummmethod.org.bd";

    $output = qm_mobile_htmlize($title, $content);

	echo $output;
}

function qm_mobile_download() {

    $title = "Quantum Download || Download Everything for Free";

    $content = "";
    $terms = qm_helper_get_terms_from_vocabulary("Publication");

    $content .= "<ul>";

    foreach($terms as $term){
        $sql = "SELECT * ";
        $sql .= "FROM {content_type_publication} AS p ";
        $sql .= "INNER JOIN {node} AS n ON p.nid = n.nid ";
        $sql .= "INNER JOIN {term_node} AS t ON p.nid = t.nid ";
        $sql .= "INNER JOIN {term_data} AS td ON t.tid = td.tid ";
        $sql .= "INNER JOIN {files} AS f ON p.field_publication_attachment_fid = f.fid ";
        $sql .= "INNER JOIN {node_revisions} nr ON n.nid = nr.nid ";
        $sql .= "WHERE t.tid = '%d' AND n.language = '%s' ";

        $query = db_query($sql, $term->tid, 'en') ;

        $exist = false;
        $newCount = 0;

        while ($data = db_fetch_object($query)) {
            $exist = true;
            if ($data->changed > strtotime('previous month')){
                $newCount++;
            }

        }

        if ($exist ){
            $content .= "<li>" . l($term->name, 'mobile/download/category/'.$term->tid);
            if($newCount){
                $content .= " (" . $newCount . " new )";
            }
            $content .= "</li>";
        }

    }
    
    $content .= "</ul>";

    $output = qm_mobile_htmlize($title, $content);

	echo $output;
}


function qm_mobile_download_by_category($tid, $sortby){

    $title = "Quantum Download || Download Everything for Free";

    $content = "";

    if ($sortby == "latest"){
        $sortby = field_pubdate_value;
    }


    $sql = "SELECT * ";
    $sql .= "FROM {content_type_publication} AS p ";
    $sql .= "INNER JOIN {node} AS n ON p.nid = n.nid ";
    $sql .= "INNER JOIN {term_node} AS t ON p.nid = t.nid ";
    $sql .= "INNER JOIN {term_data} AS td ON t.tid = td.tid ";
    $sql .= "INNER JOIN {files} AS f ON p.field_publication_attachment_fid = f.fid ";
    $sql .= "INNER JOIN {node_revisions} nr ON n.nid = nr.nid ";
    $sql .= "WHERE t.tid = '%d' AND n.language = '%s' ";
    $sql .= "ORDER BY '%s' desc ";

    $query = db_query($sql, $tid, 'en', $sortby) ;

    $content .= "<ul>";
    while ($data = db_fetch_object($query)) {
        $content .= "<li>" . l($data->title, $data->filepath );
        if ($data->changed > strtotime('previous month')){
            $content .= '<sup style="color: #ff0000">New</sup>';
        };
        $content .= "</li>";
    }
    $content .= "</ul>";

    $output = qm_mobile_htmlize($title, $content);

	echo $output;
}


function qm_mobile_event(){
    
    $title = "Quantum Events";

    $content = "<h2>Coming Soon</h2>";

    $output = qm_mobile_htmlize($title, $content);

	echo $output;

}